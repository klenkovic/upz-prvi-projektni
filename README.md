# UPZ prvi projektni zadatak

Završni projekt iz kolegija Upravljanje znanjem na [Odjelu za informatiku Sveučilišta u Rijeci].

#### Akademska godina 2015./2016.

### Autori
 * Kristijan Lenković, *univ. bacc. inf.*
 * Ivana Pilaj, *univ. bacc. inf.*


### Requirements
 * Python >= 3.2.3

### Development

Glavni dev repozitorij dostupan je preko BitBucket servisa.
```sh
$ git clone https://[username]@bitbucket.org/klenkovic/upz-prvi-projektni.git
```

### Literatura

 * https://github.com/ptwobrussell/Mining-the-Social-Web-2nd-Edition
 * https://rawgit.com/ptwobrussell/Mining-the-Social-Web-2nd-Edition/master/ipynb/html/Chapter%209%20-%20Twitter%20Cookbook.html
 * https://dev.twitter.com/overview/api/tweets
 * https://dev.twitter.com/rest/reference/get/search/tweets


[Odjelu za informatiku Sveučilišta u Rijeci]:http://www.inf.uniri.hr/

