import networkx as nx
import numpy as np
import operator as op
import matplotlib.pyplot as plt


def chunkIt(seq, num):
  avg = len(seq) / float(num)
  out = []
  last = 0.0

  while last < len(seq):
    out.append(seq[int(last):int(last + avg)])
    last += avg

  return out

def usporedba(predikcije, izbaceno):
    brojac = 0
    for p in predikcije:
        for i in izbaceno:
            if (p[0] == i[0] and p[1] == i[1]) or (p[0] == i[1] and p[1] == i[0]):
                brojac = brojac + 1
                #print("p:", p)
                #print("i:", i)
    #print("Brojac:", brojac, "Broj izbaceno:", len(izbaceno))
    return brojac/len(izbaceno)


g = nx.read_weighted_edgelist("tweets.edgelist", delimiter=";")

edges = g.edges()
k = 10

temp = chunkIt(edges,k)

postotak = 0.2
broj_veza = g.number_of_edges()
za_izbaciti = broj_veza * postotak


tocnost_prvog_algoritma = []
tocnost_drugog_algoritma = []

for i in range(0,k):
    print(i+1, ". cross validation iteracija...")
    h = g.copy()

    h.remove_edges_from(temp[i])

    predikcije_prvog_algoritma = nx.resource_allocation_index(h)
    predikcije_prvog_algoritma = list(predikcije_prvog_algoritma)
    predikcije_prvog_algoritma = sorted(predikcije_prvog_algoritma, key=op.itemgetter(2), reverse=True)

    #print(temp[i])

    #print(predikcije_prvog_algoritma[:len(temp[i])])

    predikcije_drugog_algoritma = nx.preferential_attachment(h)
    predikcije_drugog_algoritma = list(predikcije_drugog_algoritma)
    predikcije_drugog_algoritma = sorted(predikcije_drugog_algoritma, key=op.itemgetter(2), reverse=True)

    #print(predikcije_drugog_algoritma[:len(temp[i])])

    tocnost_prvog_algoritma.append(usporedba(predikcije_prvog_algoritma[:len(temp[i])], temp[i]))
    tocnost_drugog_algoritma.append(usporedba(predikcije_drugog_algoritma[:len(temp[i])], temp[i]))

#print(tocnost_prvog_algoritma)
#print(tocnost_drugog_algoritma)

prosjek_tocnosti_prvog = np.average(tocnost_prvog_algoritma)
prosjek_tocnosti_drugog = np.average(tocnost_drugog_algoritma)

print("prosjek tocnosti predikcije resource_allocation_index algoritma: ", prosjek_tocnosti_prvog *100, "%")
print("prosjek tocnosti predikcije preferential_attachment algoritma: ", prosjek_tocnosti_drugog *100, "%")

if prosjek_tocnosti_prvog == prosjek_tocnosti_drugog:
    print("algoritmi su jednaki")
else:
    if prosjek_tocnosti_prvog > prosjek_tocnosti_drugog:
        print("resource_allocation_index algoritam je bolji od preferential_attachment algoritam")
    else:
        print("preferential_attachment algoritam je bolji od resource_allocation_index algoritam")


