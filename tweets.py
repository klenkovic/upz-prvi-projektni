"""
Izvori:
https://github.com/ptwobrussell/Mining-the-Social-Web-2nd-Edition
https://rawgit.com/ptwobrussell/Mining-the-Social-Web-2nd-Edition/master/ipynb/html/Chapter%209%20-%20Twitter%20Cookbook.html
https://dev.twitter.com/overview/api/tweets
https://dev.twitter.com/rest/reference/get/search/tweets
Napomena: skripta je pisana za Python 3
"""
import networkx as nx
import twitter
import matplotlib.pyplot as plt

def oauth_login():
    # XXX: Go to http://twitter.com/apps/new to create an app and get values
    # for these credentials that you'll need to provide in place of these
    # empty string values that are defined as placeholders.
    # See https://dev.twitter.com/docs/auth/oauth for more information 
    # on Twitter's OAuth implementation.
    
    CONSUMER_KEY = 'MUHYFFT0D4M7I4R6jRS4eHPMJ'
    CONSUMER_SECRET = 'NbEEaOWu8NJAhj4mASMXBEAnmiXVRD7iOHKWpDSJTL93rHxSrQ'
    OAUTH_TOKEN = '312177056-n5f5ofUoiAZMD1sBLv71FvSy4geTlqFNogjrJfYe'
    OAUTH_TOKEN_SECRET = 'CuAZquBNmaNJBECdUUhSsj4ZxxWvINDDt5Mjes1L5U2D9'
    
    auth = twitter.oauth.OAuth(OAUTH_TOKEN, OAUTH_TOKEN_SECRET,
                               CONSUMER_KEY, CONSUMER_SECRET)
    
    twitter_api = twitter.Twitter(auth=auth)
    return twitter_api

def twitter_search(twitter_api, q, max_results=200, **kw):
    
    search_results = twitter_api.search.tweets(q=q, count=100, **kw)
    statuses = search_results["statuses"]

    # Enforce a reasonable limit
    max_results = min(1000, max_results)

    for _ in range(10): # 10*100 = 1000

        min_id = min(tweet["id"] for tweet in statuses)
        search_results = twitter_api.search.tweets(q=q, count=100, max_id=min_id-1)
        statuses += search_results["statuses"]
        
        if len(statuses) > max_results: 
            break
            
    return statuses

# Sample usage
twitter_api = oauth_login()    

# Nothing to see by displaying twitter_api except that it's now a
# defined variable

pojmovi = ["bieber", "trump", "hate", "love", "life"]
upit = " OR ".join(pojmovi)
results = twitter_search(twitter_api, upit, max_results=500) # dohvati 500 tweetova

G = nx.Graph()
for result in results: # results je lista tweetova
    for p in pojmovi:
        if p in result["text"].lower(): # da li je pojam u tekstu tweeta
            if G.has_edge(result["user"]["screen_name"], p):
                G[result["user"]["screen_name"]][p]["weight"] += 1
            else:
                G.add_edge(result["user"]["screen_name"], p, weight=1)

print("# cvorova", G.number_of_nodes())
print("# bridova", G.number_of_edges())
nx.write_weighted_edgelist(G, "tweets.edgelist", delimiter=";", encoding="utf-8")

plt.figure(999, figsize=(11, 11), dpi=200)
pos = nx.spring_layout(G)
nx.draw_networkx_nodes(G, pos, node_size=40)
nx.draw_networkx_edges(G, pos)
bridovi = dict([((u, v,), data["weight"]) for u, v, data in G.edges(data=True)])
nx.draw_networkx_edge_labels(G, pos, edge_labels=bridovi)
plt.savefig("tweets.png")
